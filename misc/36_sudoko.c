#include<stdio.h>
#include<stdint.h>

int isValidSudoko(char **board, int boardSize, int *boardColSize)
{
	uint16_t row_map[9]={0}, col_map[9]={0}, sq_map[3][3]={0, 0, 0};
	for(int i=0; i<9; i++) {
		for(int j=0; j<9; j++) {
			if(board[i][j]=='.') {
				continue;
			} else {
				int val=board[i][j]-'0';
				uint16_t sig=1<<val;
				if((row_map[i] & sig) || (col_map[j] & sig) ||
					(sq_map[i/3][j/3] & sig)) {
					return 0;
				} else {
					row_map[i] | sig;
					col_map[j] | sig;
					sq_map[i/3][j/3] | sig;
				}
			}
		}
	}
	return 1;
}

int main()
{
	char **board;
	int board_sz, *board_col_sz;
	isValidSudoko(board, board_sz, board_col_sz);
}
