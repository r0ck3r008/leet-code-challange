#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

typedef struct pixel
{
	uint16_t x, y;
	struct pixel *nxt;
} pixel;

typedef struct stack
{
	pixel *front, *back;
} stack;

void push(stack *stk, int x, int y)
{
	pixel *p=malloc(sizeof(pixel));
	p->x=x;
	p->y=y;
	p->nxt=NULL;
	if(stk->front==NULL)
		stk->front=p;
	else
		stk->back->nxt=p;

	stk->back=p;
}

pixel *top(stack *stk)
{
	return stk->front;
}

void pop(stack *stk)
{
	stk->front=stk->front->nxt;
	free(stk->front);
}

uint8_t process(uint16_t **img, uint8_t imgSz, uint8_t imgColSz, stack *stk,
			uint16_t nwcol, uint16_t olcol)
{
	pixel *p=top(stk);
	if(p==NULL)
		return 0;
	img[p->x][p->y]=nwcol;
	if((p->x!=imgSz-1) && (img[p->x+1][p->y]==olcol))
		push(stk, (p->x+1), p->y);
	if((p->y!=imgColSz-1) && (img[p->x][p->y+1]==olcol))
		push(stk, p->x, (p->y+1));
	if((p->x!=0) && (img[p->x-1][p->y])==olcol)
		push(stk, (p->x-1), p->y);
	if((p->y!=0) && (img[p->x][p->y-1]==olcol))
		push(stk, p->x, (p->y-1));

	pop(stk);
	return 1;
}

uint16_t **floodFill(uint16_t** image, uint8_t imageSize, uint8_t *imageColSize,
			uint8_t sr, uint8_t sc, uint16_t newColor,
			uint8_t *returnSize, uint8_t **retColSz)
{
	uint16_t **ret=malloc(imageSize*sizeof(uint16_t *));
	*retColSz=calloc(imageSize, sizeof(uint8_t));
	stack stk;
	stk.front=NULL;
	stk.back=NULL;
	push(&stk, sr, sc);
	uint16_t olcol=image[sr][sc];
	while(1)
		if(!process(image, imageSize, *imageColSize, &stk, newColor,
					olcol))
			break;

	return ret;
}

int8_t main(int8_t argc, int8_t **argv)
{
}
