#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdint.h>

typedef struct Trie
{
	int8_t *str;
	uint8_t word_path;
	struct Trie **ptr;
	uint16_t nchild;
} Trie;

void trieUpdate(Trie *, int8_t *, uint8_t);

Trie *trieCreate()
{
	Trie *trie=malloc(sizeof(Trie));
	trie->ptr=calloc(64, sizeof(Trie *));
	trie->nchild=0;
	trie->str=NULL;

	return trie;
}

uint16_t getCommon(int8_t *str1, int8_t *str2)
{
	uint16_t i=0;
	while(1) {
		if(str1[i]==str2[i])
			i++;
		else
			break;
	}
	return i;
}

int8_t *spliceString(int8_t *str, uint16_t size, uint16_t len)
{
	int8_t *ret=calloc(size+1, 1);
	strncpy(ret, str, size);
	if(size<len)
		ret[size]='\0';

	free(str);
	return ret;
}

Trie *spliceTrie(Trie *trie, int8_t *word, uint16_t indx)
{
	uint16_t len=strlen(trie->str);
	trieUpdate(trie, &(trie->str[indx]), trie->word_path);
	trie->str=spliceString(trie->str,
				indx, len);
	trieUpdate(trie, &word[indx], trie->word_path);
	return trie;
}

void trieUpdate(Trie *trie, int8_t *word, uint8_t word_path)
{
	uint16_t n=trie->nchild;
	uint8_t flag=0;
	for(int i=0; i<n; i++) {
		uint16_t indx=0;
		if(trie->ptr[i]!=NULL &&
		(indx=getCommon(trie->ptr[i]->str, word))) {
			flag=1;
			trie->ptr[i]=spliceTrie(trie->ptr[i], word, indx);
			break;
		} else if(trie->ptr[i]==NULL) {
			break;
		}
	}

	if(!flag && n!=64) {
		//make a new one
		Trie *new=trieCreate();
		new->str=strdup(word);
		trie->ptr[n]=new;
		trie->nchild++;
		trie->word_path=word_path;
	}
}

void trieInsert(Trie *trie, int8_t *word)
{
	int8_t *str=malloc(strlen(word)+1);
	sprintf(str, "%s$", word);
	for(uint16_t i=0; str[i]!='\0'; i++)
		trieUpdate(trie, &(str[i]), i);
	free(str);
}

Trie *search(Trie *trie, int8_t *word, uint8_t exact)
{
	if(exact && trie->str!=NULL && !strcmp(trie->str, word))
		return trie;

	uint16_t n=trie->nchild;
	Trie *curr=NULL;
	uint16_t indx=0;
	for(int i=0; i<n; i++) {
		if(trie->ptr[i]!=NULL &&
			(indx=getCommon(trie->ptr[i]->str, word))) {
			curr=trie->ptr[i];
			break;
		} else if(trie->ptr[i]==NULL) {
			break;
		}
	}
	if(indx) {
		if(exact)
			return (search(curr, word, exact));
		else if(!exact && indx==strlen(word))
			return trie;
		else
			return (search(curr, &(word[indx]), exact));
	} else {
		return NULL;
	}
}

int trieSearch(Trie *trie, int8_t *word)
{
	int8_t *term=malloc(strlen(word)+1);
	sprintf(term, "%s$", word);
	if(search(trie, term, 1)!=NULL)
		return 1;
	return 0;
}

int trieStartsWith(Trie *trie, int8_t *word)
{
	if(search(trie, word, 0)!=NULL)
		return 1;
	return 0;
}

uint8_t main(uint8_t argc, int8_t **argv)
{
	Trie *trie=trieCreate();
	trieInsert(trie, argv[1]);
	printf("Exact searched returned: %d\n", trieSearch(trie, argv[2]));
	printf("Search returned: %d\n", trieStartsWith(trie, argv[3]));
}
