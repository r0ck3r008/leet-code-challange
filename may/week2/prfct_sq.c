#include<stdio.h>
#include<stdbool.h>

//https://www.urbanpro.com/class-ix-x-tuition/fastest-way-how-to-check-if-a-number-is-a

bool isPerfectSquare(int num)
{
	if(num==1)
		return true;

	int n=num;
	int flag=1;
	while(1) {
		int sum=0;
		for(int i=0; n!=0; i++) {
			int dgt=n%10;
			if(!i) {
				if(flag==1) {
					if((dgt==2) || (dgt==3) || (dgt==7) || (dgt==8))
						return false;
					if(dgt==5)
						flag=5;
					else if(dgt==6)
						flag=6;
					else if(dgt==0)
						flag=0;
					else
						flag=-2;
				}
			} else if(i==1) {
				if(flag==5 && dgt!=2)
					return false;
				else if(flag==6 && !(dgt%2))
					return false;
				else if(flag==0 && dgt!=0)
					return false;
				else if(flag==-2 && (dgt%2))
					return false;
			}

			sum+=dgt;
			n/=10;
		}
		if(sum>9) {
			flag=-1;
			n=sum;
		} else {
			if((sum!=1) && (sum!=4) && (sum!=7) && (sum!=9))
				return false;
			break;
		}
	}

	for(int i=2; i<num; i++)
		if(!(num%i) && (i*i==num))
			return true;

	return false;
}
