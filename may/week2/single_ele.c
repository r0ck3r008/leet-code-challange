#include<stdio.h>
#include<stdint.h>

uint16_t get_half(uint16_t first, uint16_t last)
{
	uint16_t diff=last-first;
	return (diff%2) ? ((diff+1)/2) : (diff/2);
}

int16_t singleNonDuplicate(int16_t *nums, uint16_t n)
{
	uint16_t first=0, last=n-1;
	while(1) {
		uint16_t half=first+get_half(first, last);
		printf("First: %d, Last: %d, Half: %d\n", first, last, half);
		if((first==0) && (last==half))
			return nums[first];
		if(half && (nums[half-1]==nums[half])) {
			if(half%2)
				first=half;
			else
				last=half;
		} else if((half+1 < n) && (nums[half+1]==nums[half])) {
			if(!(half%2))
				first=half;
			else
				last=half;

		} else {
			return nums[half];
		}
	}
}

uint8_t main(uint8_t argc, int8_t **argv)
{
	//int16_t nums[]={1, 1, 2, 3, 3, 4, 4, 8, 8};
	//int16_t nums[]={1, 1, 2, 2, 3, 3, 4};
	int16_t nums[]={1, 2, 2, 3, 3};
	printf("The number is: %d\n", singleNonDuplicate(nums, 5));
}
