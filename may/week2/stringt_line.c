int plug(int *coords, float slope, int *base, int infinity)
{
        int y;
        if(!infinity) {
                y=((slope * coords[0]) - base[0]) + base[1];
        } else {
                if(!coords[0])
                        return 1;
                else
                        return 0;
        }
        printf("Y: %d y: %d: X: %d\n", coords[1], y, coords[0]);
        if(y==coords[1])
                return 1;
        else
                return 0;
}

bool checkStraightLine(int **coords, int n, int *col_sz)
{
        float slope;
        int infinity=0;
        if(coords[1][0]==coords[0][0])
                infinity=1;
        else
                slope=((float)coords[1][1]-coords[0][1])/((float)coords[1][0]-coords[0][0]);

        for(int i=2; i<n; i++) {
                if(!plug(coords[i], slope, coords[0], infinity))
                        return false;
        }
        return true;
}