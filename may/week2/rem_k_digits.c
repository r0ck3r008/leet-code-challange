#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

char *removeKdigits(char *num, int k)
{
	int n=strlen(num);
	if(k==n)
		return "0";
	char *res=calloc(n, sizeof(char));
	res[0]=num[0];
	for(int x=1, y=0, z=0; x<n; x++, y++) {
		if(y<k) {
			if(num[x]<res[z]) {
				// pop + push
				res[z]=num[x];
			} else if(num[x]==res[z] && (x+k)<n) {
				//push
				res[++z]=num[x];
				y--;
			}
		} else if(y>=k) {
			//push
			res[++z]=num[x];
		}
	}

	int i=0;
	while((res[i]=='0' && i++));
	if(i && !strlen(&(res[i])))
		return "0";
	return &(res[i]);
}

int main(int argc, char **argv)
{
	printf("Returned: %s\n", removeKdigits(argv[1],
					strtol(argv[2], NULL, 10)));
}
