#include<stdio.h>
#include<stdlib.h>

struct TreeNode
{
	int val;
	struct TreeNode *left, *right;
};

typedef struct Item
{
	struct TreeNode *node;
	struct Item *nxt;
} Item;

typedef struct Queue
{
	Item *front, *back;
	int num;
} Queue;

void push(Queue *que, struct TreeNode *node)
{
	Item *item=calloc(1, sizeof(item));
	item->node=node;
	if(que->front==NULL)
		que->front=item;
	else
		que->back->nxt=item;
	que->back=item;
	que->num++;
}

struct TreeNode *top(Queue *que)
{
	return que->front->node;
}

void pop(Queue *que)
{
	if(que->front==NULL)
		return;

	Item *curr=que->front;
	que->front=curr->nxt;
	que->num--;
	free(curr);
}

void inorder(Queue *que, struct TreeNode *root, int k)
{
	if(que->num==k)
		return;
	if(root->left!=NULL)
		inorder(que, root->left, k);

	push(que, root);

	if(root->right!=NULL)
		inorder(que, root->right, k);
}

int kthSmallest(struct TreeNode *root, int k)
{
	Queue que;
	inorder(&que, root, k);

	struct TreeNode *final=top(&que);
	return final->val;
}
