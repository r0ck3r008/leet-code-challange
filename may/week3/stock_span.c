#include<stdio.h>
#include<stdlib.h>

typedef struct Node
{
	int price, rec;
	struct Node *lft, *rit, *prev;
} Node;

typedef struct
{
	Node *tree, *prev;
} StockSpanner;

Node *node_init(int price)
{
	Node *node=calloc(1, sizeof(Node));
	node->price=price;
	node->rec=1;

	return node;
}

int sum_up(Node *node, int price, int rec)
{
	if(node!=NULL && node->price < price)
		return (sum_up(node->prev, price, rec+node->rec));
	else
		return rec;
}

void binTree_add_node(StockSpanner *stk, Node *root, int price)
{
	if(root==NULL) {
		stk->tree=node_init(price);
		stk->prev=stk->tree;
		return;
	}

	if(root->price > price) {
		// go left
		if(root->lft!=NULL) {
			return binTree_add_node(stk, root->lft, price);
		} else {
			Node *node=node_init(price);
			node->prev=stk->prev;
			stk->prev=node;
			root->lft=node;
			return;
		}
	} else if(root->price < price){
		// go right
		if(root->rit!=NULL) {
			return (binTree_add_node(stk, root->rit, price));
		} else {
			Node *node=node_init(price);
			node->prev=stk->prev;
			stk->prev=node;
			root->lft=node;
			return;
		}
	} else {
		root->rec++;
		if(stk->prev!=root)
			root->prev=stk->prev;
		stk->prev=root;
		return;
	}
}

void binTree_free(Node *tree)
{
	if(tree->lft!=NULL)
		binTree_free(tree->lft);

	if(tree->rit!=NULL)
		binTree_free(tree->rit);

	free(tree);
	tree=NULL;
}

StockSpanner *stockSpannerCreate()
{
	StockSpanner *stk=calloc(1, sizeof(StockSpanner));
	return stk;
}

int stockSpannerNext(StockSpanner *stk, int price)
{
	binTree_add_node(stk, stk->tree, price);
	return (sum_up(stk->prev->prev, price, 0));
}

void stockSpannerFree(StockSpanner *spanner)
{
	binTree_free(spanner->tree);
	free(spanner);
}

int main(int argc, char **argv)
{
	StockSpanner *stk=stockSpannerCreate();
	for(int i=1; i<argc; i++)
		printf("Returned: %d\n", stockSpannerNext(stk,
					strtol(argv[i], NULL, 10)));
	stockSpannerFree(stk);
}
