/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */


struct ListNode *addNode(struct ListNode *node1, struct ListNode *node2)
{
        struct ListNode *tmp=node2->next;
        node2->next=node1->next;
        node1->next=node2;
        
        return tmp;
}

struct ListNode* oddEvenList(struct ListNode* head)
{
        if(head==NULL)
                return NULL;
        else if(head->next==NULL)
                return head;
        
        struct ListNode *curr=head->next;
        struct ListNode *curr_odd=head;
        struct ListNode *curr_even=head->next;
        for(int i=2; curr!=NULL; i++) {
                if(i%2) {
                        curr_even->next=curr->next;
                        curr=addNode(curr_odd, curr);
                        curr_odd=curr_odd->next;
                } else {
                        curr_even=curr;
                        curr=curr->next;
                }
        }
        
        return head;
}