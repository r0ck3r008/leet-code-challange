#include<stdio.h>
#include<stdlib.h>

int bad_ver=0;

int isBadVersion(int n)
{
	return ((n>=bad_ver) ? (1) : (0));
}

int bisect(int a, int b)
{
	return (((a+b)%2) ? (bisect(a, b-1)): ((a+b)/2));
}

int firstbadversion(int n)
{
	int half=0, first=1, last=n;
	while(1) {
		half=bisect(first, last);
		if(isBadVersion(half)) {
			if(half==first || half==last)
				return half;
			last=half;
		} else {
			if(half==last)
				return first;
			else if(half==first)
				return last;
			first=half;
		}
	}
}

int main(int argc, char **argv)
{
	int n=(int)strtol(argv[1], NULL, 10);
	bad_ver=(int)strtol(argv[2], NULL, 10);

	printf("The first bad version is: %d\n", firstbadversion(n));
}
