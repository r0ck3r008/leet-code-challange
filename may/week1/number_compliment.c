#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

int main(int argc, char **argv)
{
	int32_t num=strtol(argv[1], NULL, 10), bkup=num;
	uint8_t i=0;
	while(bkup!=0){
		int32_t sig=1<<i++;
		bkup/=2;
		num^=sig;
	}

	printf("Number is: %d\n", num);
}
