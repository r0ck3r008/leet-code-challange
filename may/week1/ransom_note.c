#include<stdio.h>
#include<stdint.h>
#include<string.h>

int hash_it(char a)
{
	return ((int)a)%24;
}

int main(int argc, char **argv)
{
	uint8_t map[24];
	for(int i=0; i<24; i++)
		map[i]=0;


	for(int i=0; i<strlen(argv[2]); i++) {
		int indx=hash_it(argv[2][i]);
		map[indx]++;
	}

	int flag=1;
	for(int i=0; i<strlen(argv[1]); i++) {
		int indx=hash_it(argv[1][i]);
		if(!map[indx]) {
			flag=0;
			break;
		} else {
			map[indx]--;
		}
	}

	printf("Returned: %d\n", flag);
}
