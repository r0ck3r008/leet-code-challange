#include<stdio.h>
#include<stdint.h>

int16_t firstUniqChar(int8_t *str)
{
	uint32_t map1=0UL, map2=0UL;
	for(int16_t i=0; str[i]!='\0'; i++) {
		uint32_t sig=1UL<<str[i]%26;
		if(map1 & sig)
			map2 |= sig;
		else
			map1 |= sig;
	}

	for(int16_t i=0; str[i]!='\0'; i++) {
		uint32_t sig=1UL<<str[i]%26;
		if(!(map2 & sig))
			return i;
	}

	return -1;
}

int main(int argc, int8_t **argv)
{
	printf("Returned: %d\n", firstUniqChar(argv[1]));
}
