#include<stdio.h>
#include<stdint.h>
#include<string.h>

uint64_t mk_sig(char a)
{
	uint64_t sig=1ULL;
	return (sig<<(((int)a)%64));
}

void insert(uint64_t *bloom, char a)
{
	*bloom|=mk_sig(a);
}

int main(int argc, char **argv)
{
	uint64_t bloom=0ULL;
	for(int i=0; i<strlen(argv[1]); i++)
		insert(&bloom, argv[1][i]);

	int count=0;
	for(int i=0; i<strlen(argv[2]); i++) {
		uint64_t sig=mk_sig(argv[2][i]);
		if(bloom & sig)
			count++;
	}

	printf("Counted: %d\n", count);

}
