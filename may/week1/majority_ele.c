#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>

int majorityElement(int *nums, int numSize)
{
	int map[numSize];
	int half=(!(numSize%2)) ? (numSize/2) : ((numSize+1)/2);

	for(int i=0; i<numSize; i++)
		map[i]=0;

	for(int i=0; i<numSize; i++) {
		map[nums[i]%numSize]++;
		if(map[nums[i]]>=half)
			return nums[i];
	}

	return -1;
}

int8_t main(int8_t argc, int8_t **argv)
{
	int arr[]={3, 2, 3};
	printf("Returned: %d\n", majorityElement(arr, 3));
}
